# Add frequently used directory autocompletion via Z.sh
# . ~/z.sh


# If you come from bash you might have to change your $PATH.
export PATH=$PATH:$HOME/.local/bin:$HOME/bin:$HOME/.SpaceVim/bin

# Path to your oh-my-zsh installation.
# export ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
#ZSH_THEME="spaceship"

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
#HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
#DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
#plugins=(git extract virtualenv command-not-found systemd zsh-syntax-highlighting zsh-autosuggestions)

#source $ZSH/oh-my-zsh.sh
#

export ZSH_CUSTOM=$HOME/.local/share/zsh_custom

# Plugins via antigen-hs
export ANTIGEN_HS_HOME=$ZSH_CUSTOM/antigen-hs
export ANTIGEN_HS_OUT=$ZSH_CUSTOM/plugins
export ANTIGEN_HS_MY=$ZSH_CUSTOM/MyAntigen.hs
source $ANTIGEN_HS_HOME/init.zsh

# User configuration



 
fpath=( $ZSH_CUSTOM/zfunctions $fpath )

#autoload -U promptinit
#promptinit
#prompt spaceship
#


export MANPATH="$HOME/.local/share/man:/usr/local/man:$MANPATH"
export MANPAGER="$(which most)"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
  export EDITOR='nvim'
else
  export EDITOR='nvim'
fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

# From my .bashrc - not sure about full compatibility with zsh

# A shortcut function that simplifies usage of xclip.
# - Accepts input from either stdin (pipe), or params.
# ------------------------------------------------
cb() {
    local _scs_col="\e[0;32m"; local _wrn_col='\e[1;31m'; local _trn_col='\e[0;33m'
    # Check that xclip is installed.
    if ! type xclip > /dev/null 2>&1; then
        echo -e "$_wrn_col""You must have the 'xclip' program installed.\e[0m"
        # Check user is not root (root doesn't have access to user xorg server)
    elif [[ "$USER" == "root" ]]; then
        echo -e "$_wrn_col""Must be regular user (not root) to copy a file to the clipboard.\e[0m"
    else
        # If no tty, data should be available on stdin
        if ! [[ "$( tty )" == /dev/* ]]; then
            input="$(< /dev/stdin)"
        # Else, fetch input from params
        else
            input="$*"
        fi
        if [ -z "$input" ]; then  # If no input, print usage message.
            echo "Copies a string to the clipboard."
            echo "Usage: cb <string>"
            echo "       echo <string> | cb"
        else
            # Copy input to clipboard
            echo -n "$input" | xclip -selection c
            # Truncate text for status
            if [ ${#input} -gt 80 ]; then input="$(echo $input | cut -c1-80)$_trn_col...\e[0m"; fi
            # Print status.
            echo -e "$_scs_col""Copied to clipboard:\e[0m $input"
        fi
    fi
}
# Aliases / functions leveraging the cb() function
# ------------------------------------------------
# Copy contents of a file
function cbf() { cat "$1" | cb; }  
# Copy SSH public key
alias cbssh="cbf ~/.ssh/id_rsa.pub"  
# Copy current working directory
alias cbwd="pwd | cb"  
# Copy most recent command in bash history
alias cbhs="cat $HISTFILE | tail -n 1 | cb"  

if [ -f ~/.bash_aliases ]; then
    source ~/.bash_aliases
fi

export RISCV="$HOME/.local/share/riscv"
export RISCV_PATH=$RISCV
export RISCV_OPENOCD_PATH=$RISCV
export PATH=$PATH:"$HOME/.local/share/riscv/bin"


#Allow snap packages to be run from command line
export PATH=$PATH:"/var/lib/snapd/snap/bin"

#Instead of manually adding to path, let cargo (assuming rustup standard install path) figure out how to integrate itself into the shell
source $HOME/.cargo/env

#use starship prompt
eval "$(starship init zsh)"

# Enable loading of bash completions
autoload -U +X compinit && compinit
autoload -U +X bashcompinit && bashcompinit

# special autocompletes for kitty
if [ $TERM = "xterm-kitty" ]; then
    kitty + complete setup zsh | source /dev/stdin
fi

# haskellstack autocompletion
eval "$(stack --bash-completion-script stack)"

# HashiCorp Vault autocompletion
complete -o nospace -C /home/geohh/.local/bin/vault vault

# Allow vim hotkeys in readline
bindkey -v
export KEYTIMEOUT=1


#if ! { [ -n "$TMUX" ]; } then
#    tmux attach -t base || tmux new -s base
#fi

# pyenv
export PATH="/home/hutch/.pyenv/bin:$PATH"
eval "$(pyenv init -)"
export PYENV_VIRTUALENV_DISABLE_PROMPT=1
eval "$(pyenv virtualenv-init -)"

#setup spack
SPACK_ROOT="$HOME/.local/spack"
. ${SPACK_ROOT}/share/spack/setup-env.sh

# add binary archive install of tigervnc to availiable software
TIGERVNC_PATH=$HOME/.local/opt/tigervnc-1.10.1.x86_64
#check executables are present, then add to path
if [[ -f ${TIGERVNC_PATH}/usr/bin/vncviewer ]]; then
    export PATH=$PATH:${TIGERVNC_PATH}/usr/bin
fi

VIM_LATEX_PREVIEW_PATH=$HOME/.vim/plugged/vim-live-latex-preview/bin
if [[ -d ${VIM_LATEX_PREVIEW_PATH} ]]; then
    export PATH=$PATH:$VIM_LATEX_PREVIEW_PATH
fi

# note: doing this will break `spack edit`
#alias nvim="$HOME/.local/nvim.appimage"
# if [[ -n $(which wal) ]]; then
  # Use pywal, if availiable, to restore the config
  # Import colorscheme from 'wal' asynchronously
  # &   # Run the process in the background.
  # ( ) # Hide shell job control messages.
  # Not supported in the "fish" shell.
  # (cat ~/.cache/wal/sequences &)

  # Alternative (blocks terminal for 0-3ms)
  # cat ~/.cache/wal/sequences

  # To add support for TTYs this line can be optionally added.
  # source ~/.cache/wal/colors-tty.sh
#

# if [ -e /home/hutch/.nix-profile/etc/profile.d/nix.sh ]; then
  # . /home/hutch/.nix-profile/etc/profile.d/nix.sh;
# fi

