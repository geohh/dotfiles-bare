{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ExtendedDefaultRules #-}

module MyAntigen where

import Antigen

bundles =
        [ bundle "Tarrasch/zsh-functional"
        , bundle "Tarrasch/zsh-bd"
        , bundle "Tarrasch/zsh-command-not-found"
        , bundle "Tarrasch/zsh-colors"
        , bundle "StackExchange/blackbox"
        , bundle "zdharma/fast-syntax-highlighting"
        , bundle "zsh-users/zsh-autosuggestions"
        ]

config = defaultConfig {
                       plugins = bundles
                       }

main :: IO ()
main = antigen config

