alias c="clear"
alias gmpv="gnome-mpv"
alias se="sudoedit"

# Convenience alias for battery checking
alias battstat="upower -i /org/freedesktop/UPower/devices/battery_BAT0"

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
# DOES NOT WORK IN zsh due to zsh not sending commands to history until finished.
# alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'


alias vagrant21=/home/geohh/.local/bin/vagrant


# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

# It's just better
# alias ls='exa'

# Common special greps with ripgrep
alias erg='rg -e'
alias irg='rg -i'

transfer() { if [ $# -eq 0 ]; then echo -e "No arguments specified. Usage:\necho transfer /tmp/test.md\ncat /tmp/test.md | transfer test.md"; return 1; fi
tmpfile=$( mktemp -t transferXXX ); if tty -s; then basefile=$(basename "$1" | sed -e 's/[^a-zA-Z0-9._-]/-/g'); curl --progress-bar --upload-file "$1" "https://transfer.sh/$basefile" >> $tmpfile; else curl --progress-bar --upload-file "-" "https://transfer.sh/$1" >> $tmpfile ; fi; cat $tmpfile; rm -f $tmpfile; }

if [ $TERM = "xterm-kitty" ]; then
    alias icat="kitty +kitten icat"
fi

alias cfg="/usr/bin/git --git-dir=$HOME/.cfg --work-tree=$HOME"

